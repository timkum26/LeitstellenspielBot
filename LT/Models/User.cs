﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LT.Models
{
  public class User
  {
    /// <summary>
    /// Returns username. 
    /// </summary>
    public string Username { get; set; }

    /// <summary>
    /// Returns password.
    /// </summary>
    public string Password { get; set; }
  }
}
