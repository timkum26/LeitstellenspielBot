﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LT.Bot;
using LT.Models.Emergency;

namespace LT.Bots
{
    public class EmergencyBot : Bot
    {
        private Settings _settings;
        private LTActions _lta;
        private bool _loggedIn;

        private Missions _missions;

        protected override async Task Run(Settings settings)
        {
            _settings = settings;

            while (true)
            {
                if (_lta == null)
                    _lta = new LTActions(settings.Client);

                if (!_loggedIn)
                    _loggedIn = Login();

                if (_missions == null || !_missions.OwnMissions.Any())
                    _missions = GetMissions();

                if (_missions.OwnMissions.Any())
                    DoMission(_missions);

                Task.Delay(1000).Wait();
            }
        }

        private bool Login()
        {
            var doc = _lta.Login(_settings.User);

            if (doc.GetElementbyId("missions") != null)
            {
                Console.WriteLine("Login success");
                return true;
            }

            Console.WriteLine("Login failed");
            return false;
        }

        private Missions GetMissions()
        {
            return _lta.Missions();
        }

        private void DoMission(Missions missions)
        {
            var mission = missions.OwnMissions.FirstOrDefault(m => m.vehicle_state == MissionState.Idle && !m.sw);

            if (mission == null)
            {
                //TODO: Sometimes mission Min is to long (when 1 of X missions available) and a new mission appears then while sleeping
                /* var timeNow = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
                 var missionEndList = missions.OwnMissions.Where(m => m.date_end > 0).ToArray();

                 if (!missionEndList.Any())
                 {
                     Task.Delay(10000).Wait();
                     _missions = null;
                     return;
                 }

                 var missionEnd = missionEndList.Min(m => m.date_end);
                 var sleepTime = missionEnd - timeNow; //Seconds

                 if (sleepTime < 0)
                 {
                     _missions = null;
                     return;
                 }

                 Console.WriteLine($"Waiting {sleepTime} seconds...");
                 Task.Delay(TimeSpan.FromSeconds(sleepTime)).Wait();

                 return;*/

                Task.Delay(20000).Wait();
                _missions = null;
                return;
            }

            var startMission = _lta.StartMission(mission.id);

            switch (startMission)
            {
                case StartMissionState.Failed:
                    Console.WriteLine($"Mission: {mission.caption} - {mission.id} not startet successfully");
                    break;
                case StartMissionState.Started:
                    Console.WriteLine($"Mission: {mission.caption} - {mission.id} started successfully");
                    break;
                case StartMissionState.CurrentlyNoAvailableVehicle:
                    //TODO: Check arrival time of a vehicle to avoid spam from this msg.
                    HandleNoAvailableVehicle(missions);
                    break;
                default:
                    break;
            }

            _missions = null;
        }

        private void HandleNoAvailableVehicle(Missions missions)
        {
            var timeNow = DateTimeOffset.UtcNow.ToUnixTimeSeconds();

            //Check if other vehicles still drive to Mission, when yes wait
            var drivingVehicles = missions.OwnMissions.FirstOrDefault(m => m.vehicle_state == MissionState.Driving);

            var missionEndList = missions.OwnMissions
                .Where(m => m.date_end > 0 && m.vehicle_state == MissionState.Solving).ToArray();

            if (!missionEndList.Any() || drivingVehicles != null)
            {
                if (drivingVehicles != null)
                    Console.WriteLine($"No more free vehicles! Waiting till {drivingVehicles.caption} arrive");

                Task.Delay(10000).Wait();
                _missions = null;
                return;
            }

            var missionEnd = missionEndList.Min(m => m.date_end);
            var sleepTime = missionEnd - timeNow; //Seconds

            if (sleepTime < 0)
            {
                _missions = null;
                return;
            }

            Console.WriteLine($"Waiting {sleepTime} seconds...");
            Task.Delay(TimeSpan.FromSeconds(sleepTime)).Wait();

            return;
        }
    }
}
